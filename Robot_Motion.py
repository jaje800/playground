import math
import numpy as np
import matplotlib.pyplot as plt

class Robot(object):
    """ Define robot and its sensor 

    Attributes:
        pos (float): current position of robot (meters)
        vel (float): constant velocity robot will move (m/s)
        sensor_noise(float): variance in sensor accuracy

    """
    def __init__(self,pos0=0,vel=1,sensor_var=0.0):
        """ Initalize robot 

        Args:
            pos0 (float,optional): initial position of robot (meters)
                Defaults to 0.
            vel (float,optional): constant velocity robot will move (m/s)
                Defaults to 1.
            sensor_noise(float,optional): variance in sensor accuracy
                Defaults to 0.

        """
        self.pos = float(pos0)
        self.vel = float(vel)
        self.sensor_noise = math.sqrt(float(sensor_var))
    
    def getSensorReading(self):
        """ Calculates new position of robot based on its velocity and returns
        the sensor reading at that location

        Args: None

        Returns:
            Float: sensor position (m)

        """
        self.pos += self.vel
        return self.pos + np.random.randn() * self.sensor_noise

class Kalman1DFilter(object):
    """ Define 1D Kalman Filter

    Attributes:
        state (float): believed position (meters)
        var (float): believed variance in believed position 
        measure_error (float): variance in sensor accuracy
        move_error (float): variance in movement accuracy
        previous_states (array of floats): array of previous believed positions 
        previous_vars (array of floats):  array of previous believed variances

    """
    def __init__(self,pos0,var,measure_error,move_error):
        """ Initalize filter 

        Args:
            pos0 (float): initial predicted position (meters)
            var (float): inital variance in predicted position
            measure_error(float): variance in sensor accuracy
            move(float): variance in movement accuracy

        Initalizes:
            previous_states: empty list
            previous_vars: empty list

        """
        self.state = float(pos0)
        self.var = float(var)
        self.measure_error = float(measure_error)
        self.move_error = float(move_error)
        self.previous_states=[]
        self.previous_vars=[]

    def predict(self, movement):
        """ Predict position based on expected amount traveled and 
        previous state/variance.
        Stores predicited position in state attribute and
        variance in predicted position in var attribute

        Args: 
            movement(float): expected distance traveled (m)

        Returns: None 

        """
        self.state += movement
        self.var += self.move_error

    def update(self,measurement):
        """ Update position based on sensor reading 
        Stores updated predicited position in state attribute and
        variance in updtated predicted position in var attribute
        Also saves of predicted position and variance in array attributes

        Args: 
            measurement(float): reading from sensor (m)

        Returns: None 

        """
        self.state = (self.var * measurement + self.state * self.measure_error) / (self.var + self.measure_error)
        self.var = 1.0 / (1.0/self.var + 1.0/self.measure_error)
        self.previous_states.append(self.state)
        self.previous_vars.append(self.var)


if __name__ == "__main__":
    #Iterations to perform
    n=50
    #Speed of robot
    vel=0.5

    #Create a robot with ideal sensor
    perfectRobot=Robot(vel=vel, sensor_var=0.0)
    #Create a robot with sensor with variance of 3.5
    realRobot=Robot(vel=vel, sensor_var=3.5)
    #initalize Kalman filter
    kalman = Kalman1DFilter(0.0,100.0,3.5,1.0)

    #Create arrays to store sensor readings in
    perfectResults=np.zeros([n,1],dtype=float)
    realResults=np.zeros([n,1],dtype=float)
    kalmanResults=np.zeros([n,1],dtype=float)

    #Run simulation over n iterations
    for i in xrange(n):
        #Get ideal sensor position
        perfectResults[i]=perfectRobot.getSensorReading()
        #Predict reading from sensor
        kalman.predict(vel)

        #Get imperfect sensor reading
        reading=realRobot.getSensorReading()
        realResults[i]=reading
        
        #update filter based on reading
        kalman.update(reading)



    #Plot sensor readings on subplot1
    plt.subplot(211)
    plt.plot(perfectResults, 'g', label='Ideal Readings')
    plt.plot(realResults, 'r--', label='Sensor Readings')
    plt.plot(kalman.previous_states, 'b', label='Kalman Filtered Readings')
    plt.xlabel('Time (s)')
    plt.ylabel('Position (m)')
    plt.title('Sensor Readings Over Time')
    plt.legend(loc='best')
    plt.grid(1)
    #Plot Kalman Filter Variances on subplot2
    plt.subplot(212)
    plt.plot(kalman.previous_vars)
    plt.xlabel('Time (s)')
    plt.ylabel('Variance')
    plt.title('Kalman Filter Variance')
    plt.grid(1)
    #Show graph
    plt.show()

