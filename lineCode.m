%% MATLAB Miniproject
% This miniproject is intented to show my ability in crafting functional and
% readable MATLAB code. This document represents the notebook version of
% the code. Please see 
% <https://bitbucket.org/jaje800/sample-code/src/2c6e200d58c085ac63f89d5620f75eab70dc62ae/lineCode.m?at=master> 
% for the raw .m file.
%%
%% Description
% Here we will examine the spectral properties of line codes. Line codes are
% a method of creating a baseband signal that carries binary information. We
% will explore 4 codes: On/Off, Polar,Bipolar, and Manchester.
%%
% 
% * On/Off codes: a '1' is represented by a pulse and a '0' is represented
% by no transmission.
% * Polar codes: a '1' is represented by a pulse and a '0' is represented 
% by an inverse pulse.
% * Bipolar codes: a '1' is represented by a pulse that is an inverse of the
% pulse last 1 pulse sent and a '0' is represented by no transmission.
% * Manchester codes: a '1' is represented by the first half of the pulse
% high and the second half low, while a '0' is the inverse--the first half
% of the pulse low and the second half high.
% 
% In this example, we will generate 20000 random Bernoulli 1/2 distributed
% bits and encode them using a pulse length of 20 for each of the
% aforementioned codes and examine their spectral properites

%% Main Function
% Generates the required number of bits and encodes them using the four
% line code types

function main()
% MAIN  MAIN runs the miniproject to examine the spectral properties of 
%       On/Off, Polar, Bipolar, and Manchester line codes.
%
%       20000 random Bernoulli 1/2 distributed bits are generated and 
%       encoded using a pulse length of 20. The plotSpectrum function is
%       then called on each waveform to examine their properties
%

%Number of bits to generate
numBits = 20000;
%Generate numBits random bits 
bits=randi(2,1,numBits)-1;

%Rectangular pulse length in bits
pulseLen=20;

%Build a 1 pulse
onePulse = ones(1,pulseLen);
%Build a 0 pulse
zeroPulse = zeros(1,pulseLen);
%Build a -1 pulse
negOnePulse = ones(1,pulseLen)*-1;
%Build a manchester 1 pulse
manOne = [ ones(1,pulseLen/2), ones(1,pulseLen/2)*-1];
%Build a manchester 0 pulse
manZero = manOne*-1;

%Initalize arrays to hold bit sequence line codes
onOffCode = zeros(1,pulseLen*numBits);
polarCode = zeros(1,pulseLen*numBits);
bipolarCode = zeros(1,pulseLen*numBits);
machesterCode = zeros(1,pulseLen*numBits);

%Initalize state variable
lastOne=-1;
%Loop over bit sequence and encode
for i=1:length(bits)
    %Determine where in the line code to put the pulse start
    inx=(i-1)*pulseLen+1;
    %If bit is one, encode it accordingly to line code rules
    if bits(i)==1
        %For on/off code, a 1 is represented by a pulse of ones
        onOffCode(inx:inx+pulseLen-1) = onePulse;
        %For polar code, a 1 is represented by a pulse of ones
        polarCode(inx:inx+pulseLen-1) = onePulse;
        %For manchester code, a 1 is represented by a manchester one pulse
        machesterCode(inx:inx+pulseLen-1) = manOne;
        %For bipolar code, a 1 is represented by toggling the last sent pulse     
        if lastOne == -1
            bipolarCode(inx:inx+pulseLen-1)=onePulse;
            lastOne=1;
        else
            bipolarCode(inx:inx+pulseLen-1)=negOnePulse;
            lastOne=-1;
        end
    %If bit is zero, encode it accordingly to line code rules
    else
        %For on/off code, a 0 is represented by a pulse of zeros
        onOffCode(inx:inx+pulseLen-1) = zeroPulse;
        %For polar code, a 1 is represented by a pulse of negative ones
        polarCode(inx:inx+pulseLen-1) = negOnePulse;
        %For bipolar code, a 1 is represented by zeros
        bipolarCode(inx:inx+pulseLen-1)=zeroPulse;
        %For manchester code, a 1 is represented by a manchester zero pulse
        machesterCode(inx:inx+pulseLen-1) = manZero;
    end
end


%Plot the spectral proerties of each linecode's waveform
plotSpectrum(onOffCode,'On Off')
plotSpectrum(polarCode,'Polar')
plotSpectrum(bipolarCode,'Bipolar')
plotSpectrum(machesterCode,'Manchester')

end


%% Plot Spectrum Function
% This function was created to consolidate repeated code as we exmaine the
% waveforms of each of the linecodes

function plotSpectrum(waveform,plotTitle)
% PLOTSPECTRUM Given a waveform, plot first 400 samples and magnitude 
%              spectrum on a linear and decible scale and use plotTitle
%              to appropriately label graphs

%Compute Fourier transform for waveform
S=fft(waveform);
%Compute the squared magnitude spectrum
A=S.*conj(S);
%Shift zero-frequency component to center of spectrum
A=fftshift(A);
%Generate x axis for plotting
x=linspace(-pi,pi,length(A));

figure()
%Plot first 400 bits of waveform
subplot(3,1,1)
plot(waveform(1:400))
title(sprintf('%s Line Code',plotTitle))
ylabel('Bit Level')
xlabel('Bits')
axis([1 400 -1.5 1.5])

%Plot magnitude spectrum of waveform in linear units
subplot(3,1,2)
plot(x,A)
title('Magnitude Spectrum (Linear)')
ylabel('Magnitude (Linear)')
xlabel('Radians')
axis tight
%Plot magnitude spectrum of waveform in decibles
subplot(3,1,3)
plot(x,10*log(A))
title('Magnitude Spectrum (Decibles)')
ylabel('Magnitude (dB)')
xlabel('Radians')
axis tight

end
%% Results
% Above are the resulting graphs generated by this project. 
